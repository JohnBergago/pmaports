# Maintainer: Minecrell <minecrell@minecrell.net>
# Kernel config based on: https://github.com/me176c-dev/archlinux-me176c/blob/master/linux-me176c/config

_flavor="asus-me176c"
pkgname="linux-$_flavor"
pkgver=5.4.17
pkgrel=0
pkgdesc="Mainline kernel fork for ASUS MeMO Pad 7 (ME176C(X))"
arch="x86_64"
_carch="x86_64"
url="https://github.com/me176c-dev/linux-me176c"
license="GPL-2.0-only"
options="!strip !check !tracedeps"
depends_dev="perl gmp-dev elfutils-dev bash flex bison mpc1-dev mpfr-dev"
makedepends="$depends_dev sed installkernel bc linux-headers openssl-dev diffutils xz"

# Compiler: latest GCC from Alpine
HOSTCC="${CC:-gcc}"
HOSTCC="${HOSTCC#${CROSS_COMPILE}}"

# Source
source="
	$pkgname-$pkgver.tar.gz::$url/archive/$pkgver.tar.gz
	config-$_flavor.$arch
"
builddir="$srcdir/linux-me176c-$pkgver"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$arch" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION=$((pkgrel + 1 ))
}

package() {
	mkdir -p "$pkgdir"/boot
	make install modules_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir"
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="f666b41fd2b9e6775367542f0ca8f94f21232cedce71d077490e3e49dbb04566d391cdb108a1ee1fb844f34e2a7007bc2f9c384fc8ac6e361309623479ab1887  linux-asus-me176c-5.4.17.tar.gz
4e45a11e13ec1978f7954bb5977ee7defd55cf0a64a8876468d72474bd1ef2fe232f61185e770c7198bb66871028d2f4fcf511381a6270aa974008f6ed824579  config-asus-me176c.x86_64"

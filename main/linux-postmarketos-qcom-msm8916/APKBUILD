# Reference: <https://postmarketos.org/vendorkernel>
# Kernel config based on: arch/arm64/configs/msm8916_defconfig

_flavor="postmarketos-qcom-msm8916"
pkgname="linux-$_flavor"
pkgver=5.5_rc7
pkgrel=0
pkgdesc="Mainline kernel fork for Qualcomm MSM8916 devices"
arch="aarch64 armv7"
url="https://github.com/msm8916-mainline/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps"
depends_dev="perl gmp-dev elfutils-dev bash flex bison"
makedepends="$depends_dev sed installkernel bc linux-headers openssl-dev dtbtool"

# Architecture
case "$CARCH" in
	aarch64) _carch="arm64" ;;
	arm*)    _carch="arm" ;;
esac

# Compiler: latest GCC from Alpine
HOSTCC="${CC:-gcc}"
HOSTCC="${HOSTCC#${CROSS_COMPILE}}"

# Source
_tag=v${pkgver//_/-}-msm8916
source="
	$pkgname-$_tag.tar.gz::$url/archive/$_tag.tar.gz
	config-$_flavor.aarch64
	config-$_flavor.armv7
"
builddir="$srcdir/linux-${_tag#v}"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION=$((pkgrel + 1 ))
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release

	# Master DTB (deviceinfo_bootimg_qcdt)
	dtbTool -p scripts/dtc/ -o "arch/$_carch/boot"/dt.img "arch/$_carch/boot/"
	install -Dm644 "arch/$_carch/boot"/dt.img "$pkgdir"/boot/dt.img
}

sha512sums="350255503c9d1a22e094551c289c90c92df4bdde7338d72ab2d43690c6ae2ae22af2d3f87dbf6a4b049954cb18bb82075b8ffa5e5607657253dbd650df39c85b  linux-postmarketos-qcom-msm8916-v5.5-rc7-msm8916.tar.gz
d642887b33b7fee4306aeeb1324a1243f2d785776c0eeaba9d02f8772db153aa0eec6e69f0c1a5b255cba292428aa86c04b681f7ecf17861dc0863cc0b3ed520  config-postmarketos-qcom-msm8916.aarch64
810bc800b2948dcea2ca03b8d927a90832067917fe19665c4f9b1bd20a4eb7194d8b141a8462489a73ccfdd71c97f6be826e360de08aeecc29262b03fa7d1c59  config-postmarketos-qcom-msm8916.armv7"
